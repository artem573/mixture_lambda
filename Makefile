FILES=specie.cpp aux_func.cpp mixture.cpp calc_lambda.cpp reader.cpp

all:
	g++ -std=c++11 $(FILES) -o calc_lambda

clean:
	rm -rf *.o calc_lambda
