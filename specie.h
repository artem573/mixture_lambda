#ifndef _specie_h_
#define _specie_h_

#include <vector>
#include <cmath>
#include <unordered_map>
#include "aux_func.h"

enum class SpecieName { MMA, O2, N2, CO2, H2O, CO, H2, CH2O };

class Specie
{
  static const std::unordered_map<std::string, SpecieName> str_to_enum_map;
	
  const SpecieName specie_name;
	
  std::vector<double> lambda_dat;
  double T_mid; // [K]
	
  double molar_mass; // [kg / kmole]
  double molar_fract; // [-]
	
	
public:	

  Specie(const std::string &specie_name_str);
		
  double molar_fraction() const;
	
  void set_molar_fraction(double molar_fraction_value);
	
  double lambda(const double T) const;
};

#endif // _specie_h_
