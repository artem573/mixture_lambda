#include "specie.h"


const std::unordered_map<std::string, SpecieName> Specie::str_to_enum_map = 
{ 
  {"MMA", SpecieName::MMA },
  {"O2",  SpecieName::O2  },
  {"N2",  SpecieName::N2  },
  {"CO2", SpecieName::CO2 },
  {"H2O", SpecieName::H2O },
  {"CO",  SpecieName::CO  },
  {"H2",  SpecieName::H2  },
  {"CH2O", SpecieName::CH2O },
};

Specie::Specie(const std::string &specie_name_str) :
  specie_name(str_to_enum_map.at(specie_name_str)), molar_fract(0)
{
  switch(specie_name)
  {
  case SpecieName::MMA:
    lambda_dat = { 3.93937660e-01, -1.12336467e+03,  1.34041664e+05,  4.89472917e+00,
                   3.93937660e-01, -1.12336467e+03,  1.34041664e+05,  4.89472917e+00 };
    T_mid = 1000;
    molar_mass = 100;
    break;
			
  case SpecieName::O2:
    lambda_dat = { 8.0805788E-1,  1.1982181E+2, -4.7335931E+4,  9.5189193E-1,
                   8.1595343E-1, -3.4366856E+1,  2.2785080E+3,  1.0050999E+0 };
    T_mid = 1000;
    molar_mass = 32;
    break;

  case SpecieName::N2:
    lambda_dat = { 6.5147781E-1, -1.5059801E+2, -1.3746760E+4,  2.1801632E+0,
                   9.4306384E-1,  1.2279898E+2, -1.1839435E+4, -1.0668773E-1 };
    T_mid = 1000;
    molar_mass = 28;
    break;

  case SpecieName::CO2:
    lambda_dat = { 6.6068182E-1, -1.2741845E+2, -8.1580328E+4,  2.1817907E+0,
                   5.3726173E-1, -4.9928331E+2,  3.7397504E+4,  3.2903619E+0 };
    T_mid = 1000;
    molar_mass = 44;
    break;

  case SpecieName::H2O:
    lambda_dat = { 7.9349503E-1, -1.3340063E+3,  3.7884327E+5,  2.3591474E+0,
                   1.5541443E+0,  6.6106305E+1,  5.5969886E+3, -3.9259598E+0 };
    T_mid = 1000;
    molar_mass = 18;
    break;		

  case SpecieName::CO:
    lambda_dat = { 5.74936405e-01, -3.88926132e+02,  6.96651642e+04,  2.84928731e+00,
                   5.74936405e-01, -3.88926132e+02,  6.96651642e+04,  2.84928731e+00 };
    T_mid = 1000;
    molar_mass = 28;
    break;					

  case SpecieName::H2:
    lambda_dat = { 0.74368397e+00, -0.54941898e+03,  0.25676376e+06, 0.35553997e+01,
                   0.93724945e+00,  0.19013311e+03, -0.19701916e+05, 0.17545108e+01 };
    T_mid = 1000;
    molar_mass = 2;
    break;					

  case SpecieName::CH2O:
    lambda_dat = { 3.72578566e-01, -1.22945428e+03,  1.68492466e+05,  5.21715368e+00,
                   3.72578566e-01, -1.22945428e+03,  1.68492466e+05,  5.21715368e+00 };
    T_mid = 1000;
    molar_mass = 30;
    break;
  }
}
		
double Specie::molar_fraction() const { return molar_fract; }

void Specie::set_molar_fraction(double molar_fraction_value) { molar_fract = molar_fraction_value; }

double Specie::lambda(const double T) const
{
  /* ======================================================
     Calculate thermal conductivity of gas phase component
     lambda_g_i = T^A * exp(B/T + C/T^2 + D) [NASA_4513]
     Don't forget to convert units to [W/(m K)] before return (multiplyer '1e-4') 
     [NASA_4513] McBride B.J., Gordon S., Reno M.A. Coefficients for Calculating Thermodynamic and Transport Properties of Individual Species. NASA Technical Memorandum 4513, 1993.
     ====================================================== */

  if (T > T_mid) 
  { 
    return 1e-4 * pow(T, lambda_dat[0]) * exp(lambda_dat[1]/T + lambda_dat[2]/sqr(T) + lambda_dat[3]); 
  }
  else 
  { 
    return 1e-4 * pow(T, lambda_dat[4]) * exp(lambda_dat[5]/T + lambda_dat[6]/sqr(T) + lambda_dat[7]);
  }
}
