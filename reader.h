#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
#include <assert.h>
#include <unordered_map>

#include "mixture.h"

void read_and_calc(const std::string &f_in_name, const std::string &f_out_name);
