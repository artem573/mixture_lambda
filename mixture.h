#ifndef _mixture_h_
#define _mixture_h_

#include <iostream>
#include <unordered_map>
#include "specie.h"

class Mixture
{
  std::unordered_map<std::string, Specie> species;

public:	

  Mixture();
	
  void add_specie(const std::string &specie_name);

  void set_molar_fraction(const std::string &specie_name, const double value);

  // Normalize molar fractions to sum up to 1
  void normalize_specie_fractions();
	
  double lambda(const double T) const;
};

#endif // _mixture_h_

