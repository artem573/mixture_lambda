#include "reader.h"

void read_and_calc(const std::string &f_in_name, const std::string &f_out_name)
{
  Mixture mixture;
  int T_index;
	
  std::ifstream f_in;
  f_in.open(f_in_name);
	
  std::cout << "Opening '" << f_in_name << "'. \r\n";

  if (!f_in.is_open())
  {
    std::cerr << "Unable to open file " << f_in_name << " with concentrations and temperature values. \r\n";
    throw std::exception();
  }
	
  std::ofstream f_out;
  f_out.open(f_out_name);

  std::string line;
  std::stringstream ss;
	
  std::unordered_map<int, std::string> column_to_specie_map;
  
  getline(f_in, line);
  ss << line;
  for (int i = 0; ss >> line; ++i)
  {
    if (line == "T") { T_index = i; }
    else
    {
      column_to_specie_map.insert( { i, line } );
      mixture.add_specie(line);
    }
  }
  ss.str( std::string() );
  ss.clear();
	
  f_out << "lambda_mix, [W/(m K)] \r\n";
	
  while (!f_in.eof())
  {
    getline(f_in, line);
    ss << line;
    double T;
    double value;
    for (int i = 0; ss >> value; ++i)
    {
      if (i == T_index) { T = value; }
      else { mixture.set_molar_fraction(column_to_specie_map.at(i), value); }
    }
		
		mixture.normalize_specie_fractions();
    f_out << mixture.lambda(T) << "\r\n";
    ss.str( std::string() );
    ss.clear();
  }	
	
  std::cout << "Written in '" << f_out_name << "'. \r\n";
}
