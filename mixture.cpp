#include "mixture.h"

Mixture::Mixture() { };
	
void Mixture::add_specie(const std::string &specie_name_str)
{ 
  try
  {
    Specie specie(specie_name_str);
    species.insert( { specie_name_str, specie } );
  }
  catch (...)
  {
    std::cerr << "There is no thermo data for '" << specie_name_str << "' specie \r\n";
  }
}

void Mixture::set_molar_fraction(const std::string &specie_name, const double value)
{
  Specie &specie = species.at(specie_name);
  specie.set_molar_fraction(value);
}

void Mixture::normalize_specie_fractions()
{	
  double X_sum = 0;
  for (auto &specie: species)
  {
    X_sum += specie.second.molar_fraction();
  }
  for (auto &specie: species)
  {
    specie.second.set_molar_fraction(specie.second.molar_fraction() / X_sum);
  }		
}
	
double Mixture::lambda(const double T) const
{
  /* ======================================================
     Expression for mixture thermal conductivity extimation is reported in [Warnatz_2006].
     [Warnatz_2006] Warnatz J., Maas U., Dibble R.W. Combustion. Physical and Chemical Fundamentals, Modeling and Simulation, Experiments, Pollutant Formation, 2006.
     ====================================================== */

  
  double lam_1 = 0;
  double lam_2 = 0;
  for (const auto &specie: species)
  {
    double lambda_i = specie.second.lambda(T);
    lam_1 += specie.second.molar_fraction() * lambda_i;
    lam_2 += specie.second.molar_fraction() / lambda_i;
  }

  double lambda_mix = 0.5 * (lam_1 + 1.0 / lam_2);
  return lambda_mix;
}

