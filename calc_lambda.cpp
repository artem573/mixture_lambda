#include "reader.h"

int main()
{
  const std::string f_in_name = "Yi_T.dat";
  const std::string f_out_name = "lambda.dat";
  
  read_and_calc(f_in_name, f_out_name);

  std::cout << "Finished. \r\n";

  return 0;			
}
